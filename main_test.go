package main

import (
	"os"
	"testing"
)

func TestDirExists(t *testing.T) {
	home := os.Getenv("HOME")
	if !dirExists(home) {
		t.Fatalf("home is expected to exist as a folder, but it doesn't exist")
	}

	invalidDir := "/tmp/non-existing-folder"
	if dirExists(invalidDir) {
		t.Fatalf("non existing folder is considered as existing... weird")
	}

}

func TestBuildingDestinationFolder(t *testing.T) {
	tt := []struct {
		name        string
		initializer func()
		url         string
		expected    string
	}{
		{
			"valid ssh url with gopath",
			func() {
				os.Setenv("GOPATH", "/home/user/go")
			},
			"git@gitlab.com:yakshaving.art/gitclone.git",
			"/home/user/go/src/gitlab.com/yakshaving.art/gitclone",
		},
		{
			"valid git url without gopath",
			func() {
				os.Setenv("GOPATH", "")
				os.Setenv("HOME", "/home/myuser")
			},
			"git@gitlab.com:yakshaving.art/gitclone.git",
			"/home/myuser/Go/src/gitlab.com/yakshaving.art/gitclone",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tc.initializer()
			f, err := buildDestinationFolder(tc.url)
			if err != nil {
				t.Fatalf("failed to build destination folder: %s", err)
			}
			if f != tc.expected {
				t.Fatalf("destination folder %s is not as expected %s", f, tc.expected)
			}
		})
	}
}
